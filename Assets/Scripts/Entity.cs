﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace PROne
{
    public class Entity : MonoBehaviour, ILog
    {
        [SerializeField]
        string entityName;

        public string EntityName
        {
            set { entityName = value; }
            get { return entityName; }
        }

        public void Log(object message)
        {
            Debug.Log(message);
        }

    }
}