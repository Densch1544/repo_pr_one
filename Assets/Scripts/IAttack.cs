﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public interface IAttack
    {
        void AttackEnemy(Enemy enemy_Entity, int attackAmount);
    }
}