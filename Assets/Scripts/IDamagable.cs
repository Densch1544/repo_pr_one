﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public interface IDamagable
    {
        void Damage(int damageTaken);
    }
}