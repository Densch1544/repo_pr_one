﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Player : Entity, IAttack
    {
        [SerializeField]
        int currentHealth;
        [SerializeField]
        int attackAmount;
        [SerializeField]
        bool isAlive;
        [SerializeField]
        Enemy enemyEntity;

        public int CurrentHealth
        {
            set { currentHealth = value; }
            get { return currentHealth; }
        }

        public int AttackAmount
        {
            set { attackAmount = value; }
            get { return attackAmount; }
        }

        public bool IsAlive
        {
            set { isAlive = value; }
            get { return isAlive; }
        }

        public Enemy EnemyEntity
        {
            set { enemyEntity = value; }
            get { return enemyEntity; }
        }

        public void AttackEnemy(Enemy enemy_Entity, int attackAmount)
        {
            switch (enemy_Entity.IsAlive)
            {
               case true: enemy_Entity.Damage(attackAmount);
               break;

               case false: Log("This Enemy is already defeated!");
               break;
            }
        }

        void Start()
        {
            Log("The Players name is " + EntityName);
            Log("The Player has " + currentHealth + "HP");
            Log("The Player weapon is legendary and can inflict " + attackAmount + "HP as damage");
            AttackEnemy(enemyEntity, attackAmount);
        }
    }
}