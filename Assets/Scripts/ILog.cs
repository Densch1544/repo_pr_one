﻿using UnityEngine;
using System.Collections;

namespace PROne
{
    public interface ILog
    {
        void Log(object message);
    }  
}