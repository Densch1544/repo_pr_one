﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class Enemy : Entity, IDamagable 
    {
        [SerializeField]
        int currentHealth;
        [SerializeField]
        int attackAmount;
        [SerializeField]
        bool isAlive;

        public int CurrentHealth
        {
            set { currentHealth  = value; }
            get { return currentHealth; }
        }

        public int AttackAmount
        {
            set { attackAmount = value; }
            get { return attackAmount; }
        }

        public bool IsAlive
        {
            set { isAlive  = value; }
            get { return isAlive; }
        }

        public void Damage(int damageTaken)
        {
            if (currentHealth > 0)
            {
                currentHealth -= damageTaken;

                if (currentHealth <= 0)
                {
                    currentHealth = 0;
                    isAlive = false;
                    Log(EntityName + " was killed because he has " + currentHealth + "HP left!");
                    return;
                }
            }

            else if (currentHealth <= 0)
            {
                currentHealth = 0;
                isAlive = false;
                Log(EntityName + " is dead because he has " + currentHealth + "HP left!");
                return;
            }

            Log(EntityName + " was damaged and has " + currentHealth + "HP after the attack!");
        }    
    }
}