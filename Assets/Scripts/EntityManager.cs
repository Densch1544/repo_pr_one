﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PROne
{
    public class EntityManager : MonoBehaviour, ILog
    {
        public Entity[] entities; 


        public void Log(object message)
        {
            Debug.Log(message);
        }

        void Start()
        {
            Log("Names are registered");

            for(int i = 0; i < entities.Length; i++)
            {
                Log(entities[i].EntityName +" is Index Nr " + i);
            }
        }
    }
}